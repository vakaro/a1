public class Sheep {

    enum Animal {sheep, goat}

    public static void main(String[] param) {
        // for debugging
    }

    public static void reorder(Animal[] animals) {
        if (animals == null) {
            throw new RuntimeException();
        }

        int count = 0;
        for (Animal animal : animals) {
            if (animal == Animal.goat) {
                count++;
            }
        }

        for (int i = 0; i < count; i++) {
            animals[i] = Animal.goat;
        }

        for (int i = count; i < animals.length; i++) {
            animals[i] = Animal.sheep;
        }

    }
}